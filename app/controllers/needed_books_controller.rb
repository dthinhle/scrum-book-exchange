class NeededBooksController < ApplicationController

  def update
    @needed_book = NeededBook.find params[:id]
    if @needed_book.update needed_book_params
      redirect_to user_path(current_user)
    end
  end

  private

  def needed_book_params
    params.require(:needed_book).permit(:title, :author)
  end
end
