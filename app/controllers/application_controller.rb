class ApplicationController < ActionController::Base
  before_action :fetch_current_user
  before_action :configure_permitted_parameters, if: :devise_controller?

  def fetch_current_user
    if current_user
      @current_user = current_user
    end
  end

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name])
    devise_parameter_sanitizer.permit(:sign_in, keys: [:first_name, :last_name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name])
  end

end
