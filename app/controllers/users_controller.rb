class UsersController < ApplicationController

  def show
    @needed_book = @current_user.needed_books.new
  end

end
