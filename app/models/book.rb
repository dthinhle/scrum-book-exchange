class Book < ApplicationRecord

  belongs_to :user, optional: true
  belongs_to :current_owner, class_name: "User", optional: true

  enum state: [ :borrow, :sell, :exchange ]

  # validates :title
  # validates :author
  # valisates :publication_year, numericality: { only_integer: true }
end
