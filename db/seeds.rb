# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(first_name: 'Toan' , last_name: 'Ho Quang', email: 'toan@gmail.com', password: "123456", avatar: 'https://i.stack.imgur.com/ILTQq.png')
User.create(first_name: 'Hoang' , last_name: 'Nguyen Ngoc', email: 'hoang@gmail.com', password: "123456", avatar: 'https://www.freeiconspng.com/uploads/spongebob-and-patrick-png-6.png')
User.create(first_name: 'Thinh' , last_name: 'Le Duc', email: 'thinh@gmail.com', password: "123456", avatar: 'http://pluspng.com/img-png/android-png-android-logo-png-1024.png')
User.create(first_name: 'Nhat' , last_name: 'Bui Anh', email: 'nhat@gmail.com', password: "123456", avatar: 'https://atlas-content-cdn.pixelsquid.com/stock-images/cartoon-boy-harry-playing-tennis-4o7dBW5-600.jpg')
User.create(first_name: 'Duy' , last_name: 'Nguyen Duc', email: 'duy@gmail.com', password: "123456", avatar: 'http://iconbug.com/data/7a/512/b0ac6bd7f76d4605850b2d77e44ddfd9.png')
User.create(first_name: 'Hoa' , last_name: 'Vu Kieu Hai', email: 'hoa@gmail.com', password: "123456", avatar: 'http://pngimg.com/uploads/biohazard/biohazard_PNG20.png')
User.create(first_name: 'Tung' , last_name: 'Son', email: 'tung@gmail.com', password: "123456", avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQbJI1nV3aO4wCtSE618l3QL8F057Yg3iNmfbQ3cjWNZ1seBvi7')
User.create(first_name: 'Khanh' , last_name: 'Le Nguyen', email: 'khanh@gmail.com', password: "123456", avatar: 'https://www.buffalowildwings.com/globalassets/placeholders/appicon.png')
User.create(first_name: 'Trung' , last_name: 'Nguyen', email: 'trung@gmail.com', password: "123456", avatar: 'https://pngriver.com/wp-content/uploads/2018/04/Download-Hulk-PNG-Photos.png')
