class AddCurrentOwnerToBooks < ActiveRecord::Migration[5.2]
  def change
    add_reference :books, :current_owner_id, foreign_key: true
  end
end
