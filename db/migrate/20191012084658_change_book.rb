class ChangeBook < ActiveRecord::Migration[5.2]
  def change
    rename_column :books, :current_owner_id_id, :current_owner_id
    add_column :books, :summary, :string
    add_column :books, :image, :string
  end
end
