class AddAuthorToNeededBook < ActiveRecord::Migration[5.2]
  def change
    add_column :needed_books, :author, :string
  end
end
